resource "aws_sqs_queue" "import-queue" {
  name = var.queue__name
  tags = var.queue__tags
  message_retention_seconds = var.queue__message_retention_seconds
}

resource "aws_lambda_event_source_mapping" "import-queue-trigger-delegate" {
  event_source_arn = aws_sqs_queue.import-queue.arn
  function_name = aws_lambda_function.import__delegation.arn
  batch_size = var.queue__batch_size

  depends_on = [
    aws_sqs_queue.import-queue,
    aws_lambda_function.import__delegation
  ]
}
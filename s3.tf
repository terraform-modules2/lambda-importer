resource "aws_s3_bucket" "import-bucket" {
  bucket = var.s3__bucket_name

  tags = var.s3__bucket_tags
}

resource "aws_s3_bucket_notification" "lambda-reader-trigger" {
  bucket = aws_s3_bucket.import-bucket.id

  lambda_function {
    events = ["s3:ObjectCreated:*"]
    lambda_function_arn = aws_lambda_function.import__file_reader.arn
  }

  depends_on = [
    aws_s3_bucket.import-bucket,
    aws_lambda_function.import__file_reader
  ]
}
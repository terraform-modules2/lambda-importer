##########################
# Reader Stage Variables #
##########################
variable "reader__filename" {type = string}
variable "reader__function_timeout" {type = number}
variable "reader__handler" {type = string}
variable "reader__runtime" {type = string}
variable "reader__security_group_ids" {type = list(string)}
variable "reader__source_code_hash" {}
variable "reader__subnet_ids" {type = list(string)}
variable "reader__memory_size" { 
  default = 128
  type = number 
}


##########################
# Import Queue Variables #
##########################
variable "queue__name" {type = string}
variable "queue__tags" {type = map(string)}
variable "queue__batch_size" {type = number}
variable "queue__message_retention_seconds" {
  default = 3600 // One hour
}


###########################
# Import Bucket Variables #
###########################
variable "s3__bucket_name" {type = string}
variable "s3__bucket_tags" {type = map(string)}


##############################
# Delegation Stage Variables #
##############################
variable "delegate__filename" {type = string}
variable "delegate__function_timeout" {type = number}
variable "delegate__handler" {type = string}
variable "delegate__runtime" {type = string}
variable "delegate__security_group_ids" {type = list(string)}
variable "delegate__source_code_hash" {}
variable "delegate__subnet_ids" {type = list(string)}
variable "delegate__memory_size" {
  default = 128
  type = number
}

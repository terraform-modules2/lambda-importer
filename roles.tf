resource "aws_iam_role" "lambda-file-reader" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "lambda-importer-role-policy" {
  role = aws_iam_role.lambda-file-reader.id
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:CreateNetworkInterface",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface",
        "s3:Get*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  depends_on = [aws_iam_role.lambda-file-reader]
}

resource "aws_iam_role_policy_attachment" "lambda-importer-sqs" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSQSFullAccess"
  role = aws_iam_role.lambda-file-reader.name

  depends_on = [aws_iam_role.lambda-file-reader]
}

resource "aws_iam_role_policy_attachment" "lambda-importer-cloudwatch" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  role = aws_iam_role.lambda-file-reader.name

  depends_on = [aws_iam_role.lambda-file-reader]
}
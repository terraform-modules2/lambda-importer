resource "aws_lambda_function" "import__delegation" {
  function_name = "import_delegation"
  handler = var.delegate__handler
  role = aws_iam_role.lambda-file-reader.arn
  runtime = var.delegate__runtime


  source_code_hash = var.delegate__source_code_hash
  filename = var.delegate__filename

  vpc_config {
    security_group_ids = var.delegate__security_group_ids
    subnet_ids = var.delegate__subnet_ids
  }

  timeout = var.delegate__function_timeout
  memory_size = var.delegate__memory_size

  depends_on = [aws_iam_role.lambda-file-reader]
}
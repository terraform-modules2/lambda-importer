# Lambda Importer

A basic lambda import module. This utilises multiple lambda functions and AWS Step Functions to orchestrate the process of batch importing.

## Reader Function
The first step in the chain is the reader function. This function streams lines from an s3 file (e.g. a csv) and writes each line to an SQS queue. This function is invoked by the S3 `All object create events` event.

## Delegation function
This function's job is to delegate each queue message to a specific lambda function. 

## Writer function
This function's job is to write the import data to a temporary table (specific to a single import).

## Confirmation function
This functions job is to confirm the import and copy the data across form the temporary table to the live tables
resource "aws_lambda_function" "import__file_reader" {
  function_name = "import_file_reader"
  handler = var.reader__handler
  role = aws_iam_role.lambda-file-reader.arn
  runtime = var.reader__runtime

  source_code_hash = var.reader__source_code_hash
  filename = var.reader__filename

  vpc_config {
    security_group_ids = var.reader__security_group_ids
    subnet_ids = var.reader__subnet_ids
  }

  timeout = var.reader__function_timeout
  memory_size = var.reader__memory_size

  depends_on = [aws_iam_role.lambda-file-reader]
}